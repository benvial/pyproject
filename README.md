
<a class="reference external image-reference" href="https://gitlab.com/benvial/pyproject/-/releases" target="_blank"><img alt="Release" src="https://img.shields.io/endpoint?url=https://gitlab.com/benvial/pyproject/-/jobs/artifacts/main/raw/logobadge.json?job=badge&labelColor=c9c9c9"></a> 
<a class="reference external image-reference" href="https://gitlab.com/benvial/pyproject/commits/main" target="_blank"><img alt="Release" src="https://img.shields.io/gitlab/pipeline/benvial/pyproject/main?logo=gitlab&labelColor=dedede&style=for-the-badge"></a> 
<a class="reference external image-reference" href="https://benvial.gitlab.io/pyproject" target="_blank"><img alt="License" src="https://img.shields.io/badge/documentation-website-dedede.svg?logo=readthedocs&logoColor=e9d672&style=for-the-badge"></a>
<a class="reference external image-reference" href="https://gitlab.com/benvial/pyproject/commits/main" target="_blank"><img alt="Release" src="https://img.shields.io/gitlab/coverage/benvial/pyproject/main?logo=python&logoColor=e9d672&style=for-the-badge"></a>
<a class="reference external image-reference" href="https://black.readthedocs.io/en/stable/" target="_blank"><img alt="Release" src="https://img.shields.io/badge/code%20style-black-dedede.svg?logo=python&logoColor=e9d672&style=for-the-badge"></a>
<a class="reference external image-reference" href="https://gitlab.com/benvial/pyproject/-/blob/main/LICENSE.txt" target="_blank"><img alt="License" src="https://img.shields.io/badge/license-GPLv3-blue?color=aec2ff&logo=open-access&logoColor=aec2ff&style=for-the-badge"></a>


# Pyproject

**A cool package**

<!-- start elevator-pitch -->

- **Amazing feature** --- Bla bla.
- **Something else** --- blablabla bla.


<!-- end elevator-pitch -->


## Documentation

See the website with API reference and some examples at [benvial.gitlab.io/pyproject](https://benvial.gitlab.io/pyproject).



<!-- start installation -->

## Installation

### From conda/mamba


```bash 
mamba install -c conda-forge pyproject
```

### From Pypi

Simply run

```bash 
pip install pyproject
```

### From source

Clone the repository

```bash 
git clone https://gitlab.com/benvial/pyproject.git
```

Install the package locally

```bash 
cd pyproject
pip install -e .
```

For the full version:

```bash 
pip install -e .[full]
```

### From gitlab

Basic:

```bash 
pip install -e git+https://gitlab.com/benvial/pyproject.git#egg=pyproject
```



<!-- end installation -->