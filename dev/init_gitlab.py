#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import gitlab


gl = gitlab.Gitlab.from_config(None, [os.path.expanduser("~/.python-gitlab.cfg")])
name = "benvial/pyproject"

try:
    project = gl.projects.create(
        {"name": "pyproject", "visibility": "public"}
    )
except:
    project = gl.projects.get(name)

with open("justfile", "r+") as f:
    filedata = f.read()
    filedata = filedata.replace("GITLAB_PROJECT_ID_PLACEHOLDER", f"{project.id}")
    f.seek(0)
    f.write(filedata)
    f.truncate()

# project.delete()