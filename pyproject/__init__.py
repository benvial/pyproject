#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""pyproject: A cool package
"""


from .__about__ import __author__, __description__, __version__

try:
    import numdiff
    from numdiff import *
    from numdiff import _reload_package

    def set_backend(BACKEND):
        numdiff.set_backend(BACKEND)
        _reload_package("pyproject")

    def get_backend():
        return numdiff.get_backend()

    BACKEND = numdiff.BACKEND

except:
    import numpy as backend

    def set_backend(BACKEND):
        pass

    def get_backend():
        return "numpy"

    BACKEND = "numpy"


