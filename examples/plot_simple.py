#!/usr/bin/env python
# -*- coding: utf-8 -*-


"""
A simple example
==========================

Using the package.
"""

import matplotlib.pyplot as plt

import pyproject


######################################################################
# Checking results from :cite:p:`bibtextag`.


print(pyproject.__version__)